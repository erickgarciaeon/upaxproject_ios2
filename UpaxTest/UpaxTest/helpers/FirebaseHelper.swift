//
//  FirebaseHelper.swift
//  UpaxTest
//
//  Created by Luis Santiago on 31/12/21.
//

import Foundation
import FirebaseDatabase
import FirebaseStorage
import UIKit


class FirebaseAPI {
    
    var ref: DatabaseReference!
    static let shared = FirebaseAPI()
    
    private init (){}
    
    func getMainTrendings( onCompletion : @escaping()-> Void){
        
        Database.database().reference(withPath: Constants.KeyStoreProperties.controllersKey).observeSingleEvent(of: .value) { (snapshot) in
            let value = snapshot.value as? NSDictionary
            print(value)
        }
        
    }
    
    
    func uploadMedia(userName : String, imageFromGallery : UIImage?, onCompletion : @escaping(_ url : String)-> Void ){
        let storageRef = Storage.storage().reference().child("\(userName).jpg")
        if let image = imageFromGallery?.resizedTo1MB() {
            if let uploadData =  image.pngData(){
                storageRef.putData(uploadData, metadata: nil) { (extraData, error) in
                    storageRef.downloadURL(completion: { (url, error) in
                        if let urlText = url?.absoluteString {
                            onCompletion(urlText)
                        }
                    })
                }
            }
        }
    }
    
}

