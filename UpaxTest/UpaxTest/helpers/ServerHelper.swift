//
//  ServerHelper.swift
//  UpaxTest
//
//  Created by Luis Santiago on 31/12/21.
//

import Foundation

class ServerHelper {
    
    static let shared = ServerHelper()
    init(){}
    
    func getStats(onCompletion : @escaping (_ data : GeneralStatsResponse) -> Void) {
        guard let url = URL(string: Constants.baseUrl) else { return }
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else { return }
            do {
                let stats = try? JSONDecoder().decode(GeneralStatsResponse.self, from : data)
                if let response = stats{
                    onCompletion(response)
                }
                
            }
        }.resume()
    }
    
}
