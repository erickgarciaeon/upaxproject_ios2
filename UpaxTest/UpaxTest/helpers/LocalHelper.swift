//
//  LocalHelper.swift
//  UpaxTest
//
//  Created by Luis Santiago on 31/12/21.
//

import Foundation
import UIKit

class LocalHelper {
    
    static let shared = LocalHelper()
    init(){}
    
    public func setPhoto(_ image : UIImage){
        guard let data = image.jpegData(compressionQuality: 0.5) else { return }
        let encoded = try? PropertyListEncoder().encode(data)
        UserDefaults.standard.set(encoded, forKey: Constants.KeyStoreProperties.urlPictureKey)
    }
    
    public func getPhotoUrl() -> UIImage? {
        guard let data = UserDefaults.standard.data(forKey: Constants.KeyStoreProperties.urlPictureKey) else {
            return nil
        }
        
        guard let decoded = try? PropertyListDecoder().decode(Data.self, from: data) else { return nil }
        let image = UIImage(data: decoded)
        return image
    }
}
