//
//  ViewController.swift
//  UpaxTest
//
//  Created by Luis Santiago on 31/12/21.
//

import UIKit


class MenuTableViewController : UITableViewController, InputProtocol {
    
    var name : String?
    
    func onNameInserted(name: String) {
        self.name = name
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setUpTittleNav()
        tableView.keyboardDismissMode = .interactive
        initCells()
    }
    
    private func setUpTittleNav(){
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationController?.navigationItem.largeTitleDisplayMode = .always
        navigationItem.title = Constants.titleNav
    }
    
    private func initCells(){
        tableView.register(NameInputCell.self, forCellReuseIdentifier: Constants.TableKeys.inputNameCellID)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: Constants.TableKeys.galleryCellID)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: Constants.TableKeys.statsCellID)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Constants.sizeMenuDefault
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == Constants.firstItem){
            let inputCell = tableView.dequeueReusableCell(withIdentifier: Constants.TableKeys.inputNameCellID, for: indexPath) as! NameInputCell
            inputCell.inputProtocol = self
            return inputCell
        }
        
        var titleCell = Constants.TitleCells.galleryTitle
        var cellID = Constants.TableKeys.galleryCellID
        let lastItem = Constants.sizeMenuDefault - Constants.offsetItem
        
        if(indexPath.row == lastItem){
            titleCell = Constants.TitleCells.statsTitle
            cellID = Constants.TableKeys.statsCellID
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        cell.textLabel?.text = titleCell
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == Constants.firstItem){
            return CGFloat(Constants.TableHeight.inputNameCellHeight)
        }
        return CGFloat(Constants.TableHeight.defaultCellHeight)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let lastItem = Constants.sizeMenuDefault - Constants.offsetItem
        if(indexPath.row == Constants.offsetItem){
            presentModal()
        } else if(indexPath.row == lastItem){
            openStatsController()
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard section == 0 else { return nil }

        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 44.0))
        let doneButton = UIButton(type: .system)
        doneButton.frame = CGRect(x: 0, y: 0, width: 150, height: 44.0)
        doneButton.center = footerView.center
        doneButton.setTitle("Mandar Información", for: .normal)
        doneButton.layer.cornerRadius = 10.0
        doneButton.addTarget(self, action: #selector(handleSendInfo), for: .touchUpInside)
        footerView.addSubview(doneButton)

        return footerView
    }
    
    @objc func handleSendInfo(){
        if(name != nil && name?.isEmpty == false) {
            let image = LocalHelper.shared.getPhotoUrl()
            if(image != nil) {
                FirebaseAPI.shared.uploadMedia(userName: name!, imageFromGallery: image) { url in
                    self.showAlert(message: "Imagen subido con exito")
                }
            } else {
                showAlert(message: "Se necesita una imagen")
            }
        } else {
            showAlert(message: "Se necesita un nombre")
        }
    }
    
    private func showAlert(message : String) {
        let alert = UIAlertController(title: "Alerta:", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Entendido", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func openStatsController(){
        let statsTableViewController = StatsTableViewController()
        navigationController?.present(statsTableViewController, animated: true, completion: nil)
    }
    
    private func presentModal() {
        let detailViewController = DetailPhotoViewController()
        let nav = UINavigationController(rootViewController: detailViewController)
        nav.modalPresentationStyle = .pageSheet
        if let sheet = nav.sheetPresentationController {
            sheet.detents = [.medium(), .large()]
        }
        present(nav, animated: true, completion: nil)
    }
}
