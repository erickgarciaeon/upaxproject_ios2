//
//  DetailPhotoViewController.swift
//  UpaxTest
//
//  Created by Luis Santiago on 31/12/21.
//

import Foundation
import UIKit

class DetailPhotoViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var imagePicker = UIImagePickerController()

    
    let viewPictureButton : UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Ver foto", for: .normal)
        button.addTarget(self, action: #selector(handleViewPicture), for: .touchUpInside)
        return button
    }()
    
    
    let takePictureButton : UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Tomar foto", for: .normal)
        button.addTarget(self, action: #selector(handleTakePicture), for: .touchUpInside)
        return button
    }()
    
    @objc func handleViewPicture(){
        let viewPhotoController = ViewPhotoController()
        present(viewPhotoController, animated: true, completion: nil)
    }
    
    @objc func handleTakePicture(){
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = false
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        view.addSubview(takePictureButton)
        view.addSubview(viewPictureButton)
        
        
        NSLayoutConstraint.activate([
            viewPictureButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 10),
            viewPictureButton.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: 10),
            viewPictureButton.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: -10),
            viewPictureButton.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.2),
            
            takePictureButton.topAnchor.constraint(equalTo: viewPictureButton.bottomAnchor, constant: 10),
            takePictureButton.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: 10),
            takePictureButton.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: -10),
            takePictureButton.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.2)
        ])
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: nil)
        
        guard let selectedImage = info[.originalImage] as? UIImage else {
            return
        }
        
        LocalHelper.shared.setPhoto(selectedImage)
    }
    
    
    private func existLocalImage() -> Bool {
        return LocalHelper.shared.getPhotoUrl() != nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewPictureButton.isHidden = !existLocalImage()
    }
}


extension UIImage {

    func resized(withPercentage percentage: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }

    func resizedTo1MB() -> UIImage? {
        guard let imageData = self.pngData() else { return nil }
        let megaByte = 1000.0

        var resizingImage = self
        var imageSizeKB = Double(imageData.count) / megaByte // ! Or devide for 1024 if you need KB but not kB

        while imageSizeKB > megaByte { // ! Or use 1024 if you need KB but not kB
            guard let resizedImage = resizingImage.resized(withPercentage: 0.5),
            let imageData = resizedImage.pngData() else { return nil }

            resizingImage = resizedImage
            imageSizeKB = Double(imageData.count) / megaByte // ! Or devide for 1024 if you need KB but not kB
        }

        return resizingImage
    }
}
