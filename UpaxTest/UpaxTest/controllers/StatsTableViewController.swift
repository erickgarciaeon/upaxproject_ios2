//
//  StatsTableViewController.swift
//  UpaxTest
//
//  Created by Luis Santiago on 31/12/21.
//

import Foundation
import UIKit

class StatsTableViewController: UITableViewController {
    
    var questions = Array<StatQuestion>()
    var colors = Array<UIColor>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        initCells()
    }
    
    
    private func initCells(){
        tableView.register(StatsCell.self, forCellReuseIdentifier: Constants.TableKeys.statsItemCellID)
    }
    
    
    private func loadData() {
        ServerHelper.shared.getStats { data in
            if let questionsToAdd = data.questions {
                data.colors?.forEach({ color in
                    self.colors.append(hexStringToUIColor(hex: color))
                })
                
                self.questions.append(contentsOf: questionsToAdd)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.TableKeys.statsItemCellID, for: indexPath) as! StatsCell
        cell.colors = colors
        cell.questionStat = questions[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questions.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(Constants.TableHeight.statsItemCellHeight)
    }
}

func hexStringToUIColor (hex: String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt64 = 0
    Scanner(string: cString).scanHexInt64(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}
