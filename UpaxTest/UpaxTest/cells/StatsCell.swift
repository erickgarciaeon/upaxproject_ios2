//
//  StatsCell.swift
//  UpaxTest
//
//  Created by Luis Santiago on 31/12/21.
//

import Foundation
import Charts

class StatsCell: UITableViewCell, ChartViewDelegate {
  
    var colors = Array<UIColor>()
    
    var questionStat : StatQuestion? {
        didSet {
            var entries = Array<PieChartDataEntry>()
            questionStat?.chartData?.forEach({ chartData in
                if let percentage = chartData.percetnage {
                    let item = PieChartDataEntry(value: Double(percentage), label: chartData.text)
                    entries.append(item)
                }
            })
            
            let dataSet = PieChartDataSet(entries: entries, label: questionStat?.text)
            dataSet.colors = colors
            
            let data = PieChartData(dataSet: dataSet)
            data.setValueFont(.systemFont(ofSize: 3, weight: .light))
            data.setValueTextColor(.black)
            pieChart.data = data
            pieChart.highlightValues(nil)
        }
    }
    
    let pieChart : PieChartView = {
        let chart = PieChartView()
        chart.translatesAutoresizingMaskIntoConstraints = false
        return chart
    }()
    

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .white
        addSubview(pieChart)
        pieChart.delegate = self
        setUpContraints()
    }
    
    func setUpContraints() {
        NSLayoutConstraint.activate([
            pieChart.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            pieChart.leftAnchor.constraint(equalTo: leftAnchor, constant: 20),
            pieChart.rightAnchor.constraint(equalTo: rightAnchor, constant: -20),
            pieChart.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func randomColor() -> UIColor {
        let randomR: CGFloat = CGFloat(Float(arc4random()) / Float(UINT32_MAX))
        let randomG: CGFloat = CGFloat(Float(arc4random()) / Float(UINT32_MAX))
        let randomB: CGFloat = CGFloat(Float(arc4random()) / Float(UINT32_MAX))
        return UIColor(red: randomR, green: randomG, blue: randomB, alpha: 1)
    }
    
    
}

