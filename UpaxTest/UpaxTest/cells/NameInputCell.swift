//
//  NameInputCell.swift
//  UpaxTest
//
//  Created by Luis Santiago on 31/12/21.
//

import Foundation
import UIKit

class NameInputCell: UITableViewCell, UITextFieldDelegate {
    
    var inputProtocol : InputProtocol?
    
    let inputName : UITextField = {
        let input = UITextField()
        input.textColor = .black
        input.translatesAutoresizingMaskIntoConstraints = false
        input.keyboardType = .alphabet
        input.attributedPlaceholder = NSAttributedString(
            string: Constants.PlaceHolders.inputNamePlaceHolder,
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray]
        )
        return input
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .white
        selectionStyle = .none
        inputName.delegate = self
        contentView.addSubview(inputName)
        setUpConstraints()
    }
    
    func setUpConstraints(){
        NSLayoutConstraint.activate([
            inputName.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            inputName.leftAnchor.constraint(equalTo: leftAnchor, constant: 20),
            inputName.rightAnchor.constraint(equalTo: rightAnchor, constant: -10),
            inputName.heightAnchor.constraint(equalTo: heightAnchor)
        ])
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let characterSet = CharacterSet.letters
        if range.location == Constants.firstItem && string == Constants.emptyString {
            return false
        }
        if string == Constants.emptyString { return true }
        if string.rangeOfCharacter(from: characterSet.inverted) != nil {
            return false
        }
        
        if let name = textField.text {
            inputProtocol?.onNameInserted(name: name)
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let name = textField.text {
            inputProtocol?.onNameInserted(name: name)
        }
    }
    
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
