//
//  InputProtocol.swift
//  UpaxTest
//
//  Created by Luis Santiago on 31/12/21.
//

import Foundation

protocol InputProtocol {
    func onNameInserted(name : String)
}
