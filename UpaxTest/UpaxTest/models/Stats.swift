//
//  Stats.swift
//  UpaxTest
//
//  Created by Luis Santiago on 31/12/21.
//

import Foundation

struct GeneralStatsResponse : Decodable, Encodable {
    var colors : Array<String>?
    var questions : Array<StatQuestion>?
}

struct StatQuestion : Decodable, Encodable {
    var total : Int?
    var text : String?
    var chartData : Array<ChartData>?
}

struct ChartData : Decodable, Encodable{
    var text : String?
    var percetnage : Int?
}
