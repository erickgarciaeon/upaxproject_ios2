//
//  Constants.swift
//  UpaxTest
//
//  Created by Luis Santiago on 31/12/21.
//

import Foundation

struct Constants {
    
    static let sizeMenuDefault = 3
    static let firstItem = 0
    static let offsetItem = 1
    static let emptyString = " "
    static let titleNav = "EXAMEN UPAX"
    static let pictureName = "UPAX-IMAGE"
    static let baseUrl = "https://us-central1-bibliotecadecontenido.cloudfunctions.net/helloWorld"
    
    struct KeyStoreProperties {
        static let urlPictureKey = "urlPicture"
        static let controllersKey = "controllers"
    }
    
    struct PlaceHolders {
        static let inputNamePlaceHolder = "Ingresa tu nombre"
    }
    
    struct TitleCells {
        static let galleryTitle = "Galeria"
        static let statsTitle = "Graficas"
    }
    
    struct TableHeight {
        static let inputNameCellHeight = 80
        static let defaultCellHeight = 50
        static let statsItemCellHeight = 170
    }
    
    struct TableKeys {
        static let inputNameCellID = "inputNameCellID"
        static let galleryCellID = "galleryCellID"
        static let statsCellID = "statsCellID"
        static let statsItemCellID = "statsItemCellID"
    }
}
